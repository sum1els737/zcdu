/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu.xpdata;

public interface XPData {
	
	public boolean power_on();
	public boolean ac_standby_status();
	public boolean dc_standby_status();
	
	public String cdu_line00_g(String pilot);
	public String cdu_line00_i(String pilot);
	public String cdu_line00_l(String pilot);
	public String cdu_line00_m(String pilot);
	public String cdu_line00_s(String pilot);
	
	public String cdu_line01_g(String pilot);
	public String cdu_line01_i(String pilot);
	public String cdu_line01_l(String pilot);
	public String cdu_line01_m(String pilot);
	public String cdu_line01_s(String pilot);
	public String cdu_line01_x(String pilot);
	
	public String cdu_line02_g(String pilot);
	public String cdu_line02_i(String pilot);
	public String cdu_line02_l(String pilot);
	public String cdu_line02_m(String pilot);
	public String cdu_line02_s(String pilot);
	public String cdu_line02_x(String pilot);
	
	public String cdu_line03_g(String pilot);
	public String cdu_line03_i(String pilot);
	public String cdu_line03_l(String pilot);
	public String cdu_line03_m(String pilot);
	public String cdu_line03_s(String pilot);
	public String cdu_line03_x(String pilot);
	
	public String cdu_line04_g(String pilot);
	public String cdu_line04_i(String pilot);
	public String cdu_line04_l(String pilot);
	public String cdu_line04_m(String pilot);
	public String cdu_line04_s(String pilot);
	public String cdu_line04_x(String pilot);
	
	public String cdu_line05_g(String pilot);
	public String cdu_line05_i(String pilot);
	public String cdu_line05_l(String pilot);
	public String cdu_line05_m(String pilot);
	public String cdu_line05_s(String pilot);
	public String cdu_line05_x(String pilot);
	
	public String cdu_line06_g(String pilot);
	public String cdu_line06_i(String pilot);
	public String cdu_line06_l(String pilot);
	public String cdu_line06_m(String pilot);
	public String cdu_line06_s(String pilot);
	public String cdu_line06_x(String pilot);
	
	public String cdu_scratchpad(String pilot);
	
	public boolean fpln_active(String pilot);
	
	public float inst_brightness(String pilot);

}
