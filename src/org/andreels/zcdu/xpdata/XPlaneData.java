/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu.xpdata;

public class XPlaneData implements XPData {

	XPDataRepositry xpdrepo;

	public XPlaneData() {
		this.xpdrepo = XPDataRepositry.getInstance();
	}
	
	@Override
	public boolean power_on() {
		return (this.xpdrepo.cdu.power != 0);
	}

	@Override
	public boolean ac_standby_status() {
		return (this.xpdrepo.cdu.ac_standby_status != 0);
	}

	@Override
	public boolean dc_standby_status() {
		return (this.xpdrepo.cdu.dc_standby_status != 0);
	}

	//CPT
	public String cdu_line00_g(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line00_g;
		}else {
			return this.xpdrepo.cdu.cdu_2_line00_g;
		}
		
	}

	public String cdu_line00_i(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line00_i;
		}else {
			return this.xpdrepo.cdu.cdu_2_line00_i;
		}
		
	}

	public String cdu_line00_l(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line00_l;
		}else {
			return this.xpdrepo.cdu.cdu_2_line00_l;
		}

		
	}

	public String cdu_line00_m(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line00_m;
		}else {
			return this.xpdrepo.cdu.cdu_2_line00_m;
		}
		
	}

	public String cdu_line00_s(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line00_s;
		}else {
			return this.xpdrepo.cdu.cdu_2_line00_s;
		}
		
	}
	
	public String cdu_line01_g(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line01_g;
		}else {
			return this.xpdrepo.cdu.cdu_2_line01_g;
		}
		
	}

	public String cdu_line01_i(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line01_i;
		}else {
			return this.xpdrepo.cdu.cdu_2_line01_i;
		}
		
	}

	public String cdu_line01_l(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line01_l;
		}else {
			return this.xpdrepo.cdu.cdu_2_line01_l;
		}
		
	}

	public String cdu_line01_m(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line01_m;
		}else {
			return this.xpdrepo.cdu.cdu_2_line01_m;
		}
		
	}

	public String cdu_line01_s(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line01_s;
		}else {
			return this.xpdrepo.cdu.cdu_2_line01_s;
		}
		
	}
	
	public String cdu_line01_x(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line01_x;
		}else {
			return this.xpdrepo.cdu.cdu_2_line01_x;
		}
		
	}

	public String cdu_line02_g(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line02_g;
		}else {
			return this.xpdrepo.cdu.cdu_2_line02_g;
		}
		
	}

	public String cdu_line02_i(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line02_i;
		}else {
			return this.xpdrepo.cdu.cdu_2_line02_i;
		}
		
	}

	public String cdu_line02_l(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line02_l;
		}else {
			return this.xpdrepo.cdu.cdu_2_line02_l;
		}
		
	}

	public String cdu_line02_m(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line02_m;
		}else {
			return this.xpdrepo.cdu.cdu_2_line02_m;
		}
		
	}

	public String cdu_line02_s(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line02_s;
		}else {
			return this.xpdrepo.cdu.cdu_2_line02_s;
		}
		
	}

	public String cdu_line02_x(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line02_x;
		}else {
			return this.xpdrepo.cdu.cdu_2_line02_x;
		}
		
	}
	
	public String cdu_line03_g(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line03_g;
		}else {
			return this.xpdrepo.cdu.cdu_2_line03_g;
		}
		
	}

	public String cdu_line03_i(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line03_i;
		}else {
			return this.xpdrepo.cdu.cdu_2_line03_i;
		}
		
	}

	public String cdu_line03_l(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line03_l;
		}else {
			return this.xpdrepo.cdu.cdu_2_line03_l;
		}
		
	}

	public String cdu_line03_m(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line03_m;
		}else {
			return this.xpdrepo.cdu.cdu_2_line03_m;
		}
		
	}

	public String cdu_line03_s(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line03_s;
		}else {
			return this.xpdrepo.cdu.cdu_2_line03_s;
		}
		
	}

	public String cdu_line03_x(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line03_x;
		}else {
			return this.xpdrepo.cdu.cdu_2_line03_x;
		}
		
	}
	
	public String cdu_line04_g(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line04_g;
		}else {
			return this.xpdrepo.cdu.cdu_2_line04_g;
		}
		
	}

	public String cdu_line04_i(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line04_i;
		}else {
			return this.xpdrepo.cdu.cdu_2_line04_i;
		}
		
	}

	public String cdu_line04_l(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line04_l;
		}else {
			return this.xpdrepo.cdu.cdu_2_line04_l;
		}
		
	}

	public String cdu_line04_m(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line04_m;
		}else {
			return this.xpdrepo.cdu.cdu_2_line04_m;
		}
		
	}

	public String cdu_line04_s(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line04_s;
		}else {
			return this.xpdrepo.cdu.cdu_2_line04_s;
		}
		
	}

	public String cdu_line04_x(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line04_x;
		}else {
			return this.xpdrepo.cdu.cdu_2_line04_x;
		}
		
	}
	
	public String cdu_line05_g(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line05_g;
		}else {
			return this.xpdrepo.cdu.cdu_2_line05_g;
		}
		
	}

	public String cdu_line05_i(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line05_i;
		}else {
			return this.xpdrepo.cdu.cdu_2_line05_i;
		}
		
	}

	public String cdu_line05_l(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line05_l;
		}else {
			return this.xpdrepo.cdu.cdu_2_line05_l;
		}
		
	}

	public String cdu_line05_m(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line05_m;
		}else {
			return this.xpdrepo.cdu.cdu_2_line05_m;
		}
		
	}

	public String cdu_line05_s(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line05_s;
		}else {
			return this.xpdrepo.cdu.cdu_2_line05_s;
		}
		
	}

	public String cdu_line05_x(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line05_x;
		}else {
			return this.xpdrepo.cdu.cdu_2_line05_x;
		}
		
	}

	public String cdu_line06_g(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line06_g;
		}else {
			return this.xpdrepo.cdu.cdu_2_line06_g;
		}
		
	}

	public String cdu_line06_i(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line06_i;
		}else {
			return this.xpdrepo.cdu.cdu_2_line06_i;
		}
		
	}

	public String cdu_line06_l(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line06_l;
		}else {
			return this.xpdrepo.cdu.cdu_2_line06_l;
		}
		
	}

	public String cdu_line06_m(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line06_m;
		}else {
			return this.xpdrepo.cdu.cdu_2_line06_m;
		}
		
	}

	public String cdu_line06_s(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line06_s;
		}else {
			return this.xpdrepo.cdu.cdu_2_line06_s;
		}
		
	}

	public String cdu_line06_x(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_line06_x;
		}else {
			return this.xpdrepo.cdu.cdu_2_line06_x;
		}
		
	}
	
	public String cdu_scratchpad(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.cdu_1_scratchpad;
		}else {
			return this.xpdrepo.cdu.cdu_2_scratchpad;
		}
		
	}

	public boolean fpln_active(String pilot) {
		if (pilot == "CPT CDU") {
			return (this.xpdrepo.cdu.fpln_active != 0);
		}else {
			return (this.xpdrepo.cdu.fpln_active_fo != 0);
		}
	}

	public float inst_brightness(String pilot) {
		if (pilot == "CPT CDU") {
			return this.xpdrepo.cdu.inst_brightness[0];
		}else {
			return this.xpdrepo.cdu.inst_brightness[1];
		}
	}

	
}
