/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu.xpdata;

import java.util.Base64;

import org.andreels.zcdu.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zcdu.ExtPlaneInterface.data.DataRef;
import org.andreels.zcdu.ExtPlaneInterface.util.Observer;


public class CDU {

	ExtPlaneInterface iface;
	
	//power
	private final String POWER = "laminar/B738/electric/batbus_status"; 
	private final String AC_STANDBY_STATUS = "laminar/B738/electric/ac_stdbus_status"; // Captain DUs and ENGINE shown
	private final String DC_STANDBY_STATUS = "laminar/B738/electric/dc_stdbus_status"; // FO DUs and MFD shown

	//CPT
	private final String CDU_1_LINE00_G = "laminar/B738/fmc1/Line00_G";
	private final String CDU_1_LINE00_I = "laminar/B738/fmc1/Line00_I";
	private final String CDU_1_LINE00_L = "laminar/B738/fmc1/Line00_L";
	private final String CDU_1_LINE00_M = "laminar/B738/fmc1/Line00_M";
	private final String CDU_1_LINE00_S = "laminar/B738/fmc1/Line00_S";

	private final String CDU_1_LINE01_G = "laminar/B738/fmc1/Line01_G";
	private final String CDU_1_LINE01_I = "laminar/B738/fmc1/Line01_I";
	private final String CDU_1_LINE01_L = "laminar/B738/fmc1/Line01_L";
	private final String CDU_1_LINE01_M = "laminar/B738/fmc1/Line01_M";
	private final String CDU_1_LINE01_S = "laminar/B738/fmc1/Line01_S";
	private final String CDU_1_LINE01_X = "laminar/B738/fmc1/Line01_X";

	private final String CDU_1_LINE02_G = "laminar/B738/fmc1/Line02_G";
	private final String CDU_1_LINE02_I = "laminar/B738/fmc1/Line02_I";
	private final String CDU_1_LINE02_L = "laminar/B738/fmc1/Line02_L";
	private final String CDU_1_LINE02_M = "laminar/B738/fmc1/Line02_M";
	private final String CDU_1_LINE02_S = "laminar/B738/fmc1/Line02_S";
	private final String CDU_1_LINE02_X = "laminar/B738/fmc1/Line02_X";

	private final String CDU_1_LINE03_G = "laminar/B738/fmc1/Line03_G";
	private final String CDU_1_LINE03_I = "laminar/B738/fmc1/Line03_I";
	private final String CDU_1_LINE03_L = "laminar/B738/fmc1/Line03_L";
	private final String CDU_1_LINE03_M = "laminar/B738/fmc1/Line03_M";
	private final String CDU_1_LINE03_S = "laminar/B738/fmc1/Line03_S";
	private final String CDU_1_LINE03_X = "laminar/B738/fmc1/Line03_X";

	private final String CDU_1_LINE04_G = "laminar/B738/fmc1/Line04_G";
	private final String CDU_1_LINE04_I = "laminar/B738/fmc1/Line04_I";
	private final String CDU_1_LINE04_L = "laminar/B738/fmc1/Line04_L";
	private final String CDU_1_LINE04_M = "laminar/B738/fmc1/Line04_M";
	private final String CDU_1_LINE04_S = "laminar/B738/fmc1/Line04_S";
	private final String CDU_1_LINE04_X = "laminar/B738/fmc1/Line04_X";

	private final String CDU_1_LINE05_G = "laminar/B738/fmc1/Line05_G";
	private final String CDU_1_LINE05_I = "laminar/B738/fmc1/Line05_I";
	private final String CDU_1_LINE05_L = "laminar/B738/fmc1/Line05_L";
	private final String CDU_1_LINE05_M = "laminar/B738/fmc1/Line05_M";
	private final String CDU_1_LINE05_S = "laminar/B738/fmc1/Line05_S";
	private final String CDU_1_LINE05_X = "laminar/B738/fmc1/Line05_X";

	private final String CDU_1_LINE06_G = "laminar/B738/fmc1/Line06_G";
	private final String CDU_1_LINE06_I = "laminar/B738/fmc1/Line06_I";
	private final String CDU_1_LINE06_L = "laminar/B738/fmc1/Line06_L";
	private final String CDU_1_LINE06_M = "laminar/B738/fmc1/Line06_M";
	private final String CDU_1_LINE06_S = "laminar/B738/fmc1/Line06_S";
	private final String CDU_1_LINE06_X = "laminar/B738/fmc1/Line06_X";

	private final String CDU_1_SCRATCHPAD = "laminar/B738/fmc1/Line_entry";

	private final String FPLN_ACTIVE = "laminar/B738/fms/fpln_acive";

	//FO
	private final String CDU_2_LINE00_G = "laminar/B738/fmc2/Line00_G";
	private final String CDU_2_LINE00_I = "laminar/B738/fmc2/Line00_I";
	private final String CDU_2_LINE00_L = "laminar/B738/fmc2/Line00_L";
	private final String CDU_2_LINE00_M = "laminar/B738/fmc2/Line00_M";
	private final String CDU_2_LINE00_S = "laminar/B738/fmc2/Line00_S";

	private final String CDU_2_LINE01_G = "laminar/B738/fmc2/Line01_G";
	private final String CDU_2_LINE01_I = "laminar/B738/fmc2/Line01_I";
	private final String CDU_2_LINE01_L = "laminar/B738/fmc2/Line01_L";
	private final String CDU_2_LINE01_M = "laminar/B738/fmc2/Line01_M";
	private final String CDU_2_LINE01_S = "laminar/B738/fmc2/Line01_S";
	private final String CDU_2_LINE01_X = "laminar/B738/fmc2/Line01_X";

	private final String CDU_2_LINE02_G = "laminar/B738/fmc2/Line02_G";
	private final String CDU_2_LINE02_I = "laminar/B738/fmc2/Line02_I";
	private final String CDU_2_LINE02_L = "laminar/B738/fmc2/Line02_L";
	private final String CDU_2_LINE02_M = "laminar/B738/fmc2/Line02_M";
	private final String CDU_2_LINE02_S = "laminar/B738/fmc2/Line02_S";
	private final String CDU_2_LINE02_X = "laminar/B738/fmc2/Line02_X";

	private final String CDU_2_LINE03_G = "laminar/B738/fmc2/Line03_G";
	private final String CDU_2_LINE03_I = "laminar/B738/fmc2/Line03_I";
	private final String CDU_2_LINE03_L = "laminar/B738/fmc2/Line03_L";
	private final String CDU_2_LINE03_M = "laminar/B738/fmc2/Line03_M";
	private final String CDU_2_LINE03_S = "laminar/B738/fmc2/Line03_S";
	private final String CDU_2_LINE03_X = "laminar/B738/fmc2/Line03_X";

	private final String CDU_2_LINE04_G = "laminar/B738/fmc2/Line04_G";
	private final String CDU_2_LINE04_I = "laminar/B738/fmc2/Line04_I";
	private final String CDU_2_LINE04_L = "laminar/B738/fmc2/Line04_L";
	private final String CDU_2_LINE04_M = "laminar/B738/fmc2/Line04_M";
	private final String CDU_2_LINE04_S = "laminar/B738/fmc2/Line04_S";
	private final String CDU_2_LINE04_X = "laminar/B738/fmc2/Line04_X";

	private final String CDU_2_LINE05_G = "laminar/B738/fmc2/Line05_G";
	private final String CDU_2_LINE05_I = "laminar/B738/fmc2/Line05_I";
	private final String CDU_2_LINE05_L = "laminar/B738/fmc2/Line05_L";
	private final String CDU_2_LINE05_M = "laminar/B738/fmc2/Line05_M";
	private final String CDU_2_LINE05_S = "laminar/B738/fmc2/Line05_S";
	private final String CDU_2_LINE05_X = "laminar/B738/fmc2/Line05_X";

	private final String CDU_2_LINE06_G = "laminar/B738/fmc2/Line06_G";
	private final String CDU_2_LINE06_I = "laminar/B738/fmc2/Line06_I";
	private final String CDU_2_LINE06_L = "laminar/B738/fmc2/Line06_L";
	private final String CDU_2_LINE06_M = "laminar/B738/fmc2/Line06_M";
	private final String CDU_2_LINE06_S = "laminar/B738/fmc2/Line06_S";
	private final String CDU_2_LINE06_X = "laminar/B738/fmc2/Line06_X";

	private final String CDU_2_SCRATCHPAD = "laminar/B738/fmc2/Line_entry";

	private final String FPLN_ACTIVE_FO = "laminar/B738/fms/fpln_acive_fo";

	private final String INST_BRIGHTNESS = "laminar/B738/electric/instrument_brightness";

	public int power = 0;
	public int ac_standby_status = 0;
	public int dc_standby_status = 0;
	public float[] inst_brightness = new float[2];

	//CPT
	public String cdu_1_line00_g = " ";
	public String cdu_1_line00_i = " ";
	public String cdu_1_line00_l = " ";
	public String cdu_1_line00_m = " ";
	public String cdu_1_line00_s = " ";

	public String cdu_1_line01_g = " ";
	public String cdu_1_line01_i = " ";
	public String cdu_1_line01_l = " ";
	public String cdu_1_line01_m = " ";
	public String cdu_1_line01_s = " ";
	public String cdu_1_line01_x = " ";

	public String cdu_1_line02_g = " ";
	public String cdu_1_line02_i = " ";
	public String cdu_1_line02_l = " ";
	public String cdu_1_line02_m = " ";
	public String cdu_1_line02_s = " ";
	public String cdu_1_line02_x = " ";

	public String cdu_1_line03_g = " ";
	public String cdu_1_line03_i = " ";
	public String cdu_1_line03_l = " ";
	public String cdu_1_line03_m = " ";
	public String cdu_1_line03_s = " ";
	public String cdu_1_line03_x = " ";

	public String cdu_1_line04_g = " ";
	public String cdu_1_line04_i = " ";
	public String cdu_1_line04_l = " ";
	public String cdu_1_line04_m = " ";
	public String cdu_1_line04_s = " ";
	public String cdu_1_line04_x = " ";

	public String cdu_1_line05_g = " ";
	public String cdu_1_line05_i = " ";
	public String cdu_1_line05_l = " ";
	public String cdu_1_line05_m = " ";
	public String cdu_1_line05_s = " ";
	public String cdu_1_line05_x = " ";

	public String cdu_1_line06_g = " ";
	public String cdu_1_line06_i = " ";
	public String cdu_1_line06_l = " ";
	public String cdu_1_line06_m = " ";
	public String cdu_1_line06_s = " ";
	public String cdu_1_line06_x = " ";

	public String cdu_1_scratchpad = " ";

	public int fpln_active = 0;

	//FO
	public String cdu_2_line00_g = " ";
	public String cdu_2_line00_i = " ";
	public String cdu_2_line00_l = " ";
	public String cdu_2_line00_m = " ";
	public String cdu_2_line00_s = " ";

	public String cdu_2_line01_g = " ";
	public String cdu_2_line01_i = " ";
	public String cdu_2_line01_l = " ";
	public String cdu_2_line01_m = " ";
	public String cdu_2_line01_s = " ";
	public String cdu_2_line01_x = " ";

	public String cdu_2_line02_g = " ";
	public String cdu_2_line02_i = " ";
	public String cdu_2_line02_l = " ";
	public String cdu_2_line02_m = " ";
	public String cdu_2_line02_s = " ";
	public String cdu_2_line02_x = " ";

	public String cdu_2_line03_g = " ";
	public String cdu_2_line03_i = " ";
	public String cdu_2_line03_l = " ";
	public String cdu_2_line03_m = " ";
	public String cdu_2_line03_s = " ";
	public String cdu_2_line03_x = " ";

	public String cdu_2_line04_g = " ";
	public String cdu_2_line04_i = " ";
	public String cdu_2_line04_l = " ";
	public String cdu_2_line04_m = " ";
	public String cdu_2_line04_s = " ";
	public String cdu_2_line04_x = " ";

	public String cdu_2_line05_g = " ";
	public String cdu_2_line05_i = " ";
	public String cdu_2_line05_l = " ";
	public String cdu_2_line05_m = " ";
	public String cdu_2_line05_s = " ";
	public String cdu_2_line05_x = " ";

	public String cdu_2_line06_g = " ";
	public String cdu_2_line06_i = " ";
	public String cdu_2_line06_l = " ";
	public String cdu_2_line06_m = " ";
	public String cdu_2_line06_s = " ";
	public String cdu_2_line06_x = " ";

	public String cdu_2_scratchpad = " ";

	public int fpln_active_fo = 0;

	public CDU(ExtPlaneInterface iface) {
		this.iface = iface;
	}
	
	public void subscribeDrefs() {
		
		Observer<DataRef> cdu1_lines = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				switch (object.getName()) {
				case CDU_1_LINE00_G:
					cdu_1_line00_g = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE00_I:
					cdu_1_line00_i = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE00_L:
					cdu_1_line00_l = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE00_M:
					cdu_1_line00_m = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE00_S:
					cdu_1_line00_s = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE01_G:
					cdu_1_line01_g = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE01_I:
					cdu_1_line01_i = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE01_L:
					cdu_1_line01_l = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE01_M:
					cdu_1_line01_m = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE01_S:
					cdu_1_line01_s = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE01_X:
					cdu_1_line01_x = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE02_G:
					cdu_1_line02_g = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE02_I:
					cdu_1_line02_i = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE02_L:
					cdu_1_line02_l = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE02_M:
					cdu_1_line02_m = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE02_S:
					cdu_1_line02_s = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE02_X:
					cdu_1_line02_x = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE03_G:
					cdu_1_line03_g = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE03_I:
					cdu_1_line03_i = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE03_L:
					cdu_1_line03_l = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE03_M:
					cdu_1_line03_m = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE03_S:
					cdu_1_line03_s = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE03_X:
					cdu_1_line03_x = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE04_G:
					cdu_1_line04_g = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE04_I:
					cdu_1_line04_i = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE04_L:
					cdu_1_line04_l = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE04_M:
					cdu_1_line04_m = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE04_S:
					cdu_1_line04_s = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE04_X:
					cdu_1_line04_x = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE05_G:
					cdu_1_line05_g = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE05_I:
					cdu_1_line05_i = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE05_L:
					cdu_1_line05_l = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE05_M:
					cdu_1_line05_m = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE05_S:
					cdu_1_line05_s = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE05_X:
					cdu_1_line05_x = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE06_G:
					cdu_1_line06_g = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE06_I:
					cdu_1_line06_i = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE06_L:
					cdu_1_line06_l = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE06_M:
					cdu_1_line06_m = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE06_S:
					cdu_1_line06_s = decode(object.getValue()[0]);
					break;
				case CDU_1_LINE06_X:
					cdu_1_line06_x = decode(object.getValue()[0]);
					break;
				case CDU_1_SCRATCHPAD:
					cdu_1_scratchpad = decode(object.getValue()[0]);
					break;
				case FPLN_ACTIVE:
					fpln_active = Integer.parseInt(object.getValue()[0]);
					break;
				}

			}
		};
		iface.includeDataRef(CDU_1_LINE00_G);
		iface.includeDataRef(CDU_1_LINE00_I);
		iface.includeDataRef(CDU_1_LINE00_L);
		iface.includeDataRef(CDU_1_LINE00_M);
		iface.includeDataRef(CDU_1_LINE00_S);
		iface.includeDataRef(CDU_1_LINE01_G);
		iface.includeDataRef(CDU_1_LINE01_I);
		iface.includeDataRef(CDU_1_LINE01_L);
		iface.includeDataRef(CDU_1_LINE01_M);
		iface.includeDataRef(CDU_1_LINE01_S);
		iface.includeDataRef(CDU_1_LINE01_X);
		iface.includeDataRef(CDU_1_LINE02_G);
		iface.includeDataRef(CDU_1_LINE02_I);
		iface.includeDataRef(CDU_1_LINE02_L);
		iface.includeDataRef(CDU_1_LINE02_M);
		iface.includeDataRef(CDU_1_LINE02_S);
		iface.includeDataRef(CDU_1_LINE02_X);
		iface.includeDataRef(CDU_1_LINE03_G);
		iface.includeDataRef(CDU_1_LINE03_I);
		iface.includeDataRef(CDU_1_LINE03_L);
		iface.includeDataRef(CDU_1_LINE03_M);
		iface.includeDataRef(CDU_1_LINE03_S);
		iface.includeDataRef(CDU_1_LINE03_X);
		iface.includeDataRef(CDU_1_LINE04_G);
		iface.includeDataRef(CDU_1_LINE04_I);
		iface.includeDataRef(CDU_1_LINE04_L);
		iface.includeDataRef(CDU_1_LINE04_M);
		iface.includeDataRef(CDU_1_LINE04_S);
		iface.includeDataRef(CDU_1_LINE04_X);
		iface.includeDataRef(CDU_1_LINE05_G);
		iface.includeDataRef(CDU_1_LINE05_I);
		iface.includeDataRef(CDU_1_LINE05_L);
		iface.includeDataRef(CDU_1_LINE05_M);
		iface.includeDataRef(CDU_1_LINE05_S);
		iface.includeDataRef(CDU_1_LINE05_X);
		iface.includeDataRef(CDU_1_LINE06_G);
		iface.includeDataRef(CDU_1_LINE06_I);
		iface.includeDataRef(CDU_1_LINE06_L);
		iface.includeDataRef(CDU_1_LINE06_M);
		iface.includeDataRef(CDU_1_LINE06_S);
		iface.includeDataRef(CDU_1_LINE06_X);
		iface.includeDataRef(CDU_1_SCRATCHPAD);
		iface.includeDataRef(FPLN_ACTIVE);
		iface.observeDataRef(CDU_1_LINE00_G, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE00_I, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE00_L, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE00_M, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE00_S, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE01_G, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE01_I, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE01_L, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE01_M, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE01_S, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE01_X, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE02_G, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE02_I, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE02_L, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE02_M, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE02_S, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE02_X, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE03_G, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE03_I, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE03_L, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE03_M, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE03_S, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE03_X, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE04_G, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE04_I, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE04_L, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE04_M, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE04_S, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE04_X, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE05_G, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE05_I, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE05_L, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE05_M, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE05_S, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE05_X, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE06_G, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE06_I, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE06_L, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE06_M, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE06_S, cdu1_lines);
		iface.observeDataRef(CDU_1_LINE06_X, cdu1_lines);
		iface.observeDataRef(CDU_1_SCRATCHPAD, cdu1_lines);
		iface.observeDataRef(FPLN_ACTIVE, cdu1_lines);


		Observer<DataRef> cdu2_lines = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				switch (object.getName()) {
				case CDU_2_LINE00_G:
					cdu_2_line00_g = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE00_I:
					cdu_2_line00_i = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE00_L:
					cdu_2_line00_l = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE00_M:
					cdu_2_line00_m = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE00_S:
					cdu_2_line00_s = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE01_G:
					cdu_2_line01_g = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE01_I:
					cdu_2_line01_i = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE01_L:
					cdu_2_line01_l = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE01_M:
					cdu_2_line01_m = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE01_S:
					cdu_2_line01_s = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE01_X:
					cdu_2_line01_x = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE02_G:
					cdu_2_line02_g = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE02_I:
					cdu_2_line02_i = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE02_L:
					cdu_2_line02_l = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE02_M:
					cdu_2_line02_m = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE02_S:
					cdu_2_line02_s = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE02_X:
					cdu_2_line02_x = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE03_G:
					cdu_2_line03_g = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE03_I:
					cdu_2_line03_i = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE03_L:
					cdu_2_line03_l = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE03_M:
					cdu_2_line03_m = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE03_S:
					cdu_2_line03_s = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE03_X:
					cdu_2_line03_x = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE04_G:
					cdu_2_line04_g = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE04_I:
					cdu_2_line04_i = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE04_L:
					cdu_2_line04_l = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE04_M:
					cdu_2_line04_m = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE04_S:
					cdu_2_line04_s = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE04_X:
					cdu_2_line04_x = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE05_G:
					cdu_2_line05_g = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE05_I:
					cdu_2_line05_i = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE05_L:
					cdu_2_line05_l = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE05_M:
					cdu_2_line05_m = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE05_S:
					cdu_2_line05_s = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE05_X:
					cdu_2_line05_x = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE06_G:
					cdu_2_line06_g = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE06_I:
					cdu_2_line06_i = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE06_L:
					cdu_2_line06_l = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE06_M:
					cdu_2_line06_m = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE06_S:
					cdu_2_line06_s = decode(object.getValue()[0]);
					break;
				case CDU_2_LINE06_X:
					cdu_2_line06_x = decode(object.getValue()[0]);
					break;
				case CDU_2_SCRATCHPAD:
					cdu_2_scratchpad = decode(object.getValue()[0]);
					break;
				case FPLN_ACTIVE_FO:
					fpln_active_fo = Integer.parseInt(object.getValue()[0]);
					break;
				}

			}
		};
		iface.includeDataRef(CDU_2_LINE00_G);
		iface.includeDataRef(CDU_2_LINE00_I);
		iface.includeDataRef(CDU_2_LINE00_L);
		iface.includeDataRef(CDU_2_LINE00_M);
		iface.includeDataRef(CDU_2_LINE00_S);
		iface.includeDataRef(CDU_2_LINE01_G);
		iface.includeDataRef(CDU_2_LINE01_I);
		iface.includeDataRef(CDU_2_LINE01_L);
		iface.includeDataRef(CDU_2_LINE01_M);
		iface.includeDataRef(CDU_2_LINE01_S);
		iface.includeDataRef(CDU_2_LINE01_X);
		iface.includeDataRef(CDU_2_LINE02_G);
		iface.includeDataRef(CDU_2_LINE02_I);
		iface.includeDataRef(CDU_2_LINE02_L);
		iface.includeDataRef(CDU_2_LINE02_M);
		iface.includeDataRef(CDU_2_LINE02_S);
		iface.includeDataRef(CDU_2_LINE02_X);
		iface.includeDataRef(CDU_2_LINE03_G);
		iface.includeDataRef(CDU_2_LINE03_I);
		iface.includeDataRef(CDU_2_LINE03_L);
		iface.includeDataRef(CDU_2_LINE03_M);
		iface.includeDataRef(CDU_2_LINE03_S);
		iface.includeDataRef(CDU_2_LINE03_X);
		iface.includeDataRef(CDU_2_LINE04_G);
		iface.includeDataRef(CDU_2_LINE04_I);
		iface.includeDataRef(CDU_2_LINE04_L);
		iface.includeDataRef(CDU_2_LINE04_M);
		iface.includeDataRef(CDU_2_LINE04_S);
		iface.includeDataRef(CDU_2_LINE04_X);
		iface.includeDataRef(CDU_2_LINE05_G);
		iface.includeDataRef(CDU_2_LINE05_I);
		iface.includeDataRef(CDU_2_LINE05_L);
		iface.includeDataRef(CDU_2_LINE05_M);
		iface.includeDataRef(CDU_2_LINE05_S);
		iface.includeDataRef(CDU_2_LINE05_X);
		iface.includeDataRef(CDU_2_LINE06_G);
		iface.includeDataRef(CDU_2_LINE06_I);
		iface.includeDataRef(CDU_2_LINE06_L);
		iface.includeDataRef(CDU_2_LINE06_M);
		iface.includeDataRef(CDU_2_LINE06_S);
		iface.includeDataRef(CDU_2_LINE06_X);
		iface.includeDataRef(CDU_2_SCRATCHPAD);
		iface.includeDataRef(FPLN_ACTIVE_FO);
		iface.observeDataRef(CDU_2_LINE00_G, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE00_I, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE00_L, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE00_M, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE00_S, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE01_G, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE01_I, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE01_L, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE01_M, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE01_S, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE01_X, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE02_G, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE02_I, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE02_L, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE02_M, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE02_S, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE02_X, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE03_G, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE03_I, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE03_L, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE03_M, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE03_S, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE03_X, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE04_G, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE04_I, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE04_L, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE04_M, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE04_S, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE04_X, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE05_G, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE05_I, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE05_L, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE05_M, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE05_S, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE05_X, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE06_G, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE06_I, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE06_L, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE06_M, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE06_S, cdu2_lines);
		iface.observeDataRef(CDU_2_LINE06_X, cdu2_lines);
		iface.observeDataRef(CDU_2_SCRATCHPAD, cdu2_lines);
		iface.observeDataRef(FPLN_ACTIVE_FO, cdu2_lines);


		Observer<DataRef> generic = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				switch(object.getName()) {
				case INST_BRIGHTNESS:
					inst_brightness[0] = Float.parseFloat(object.getValue()[10]);
					inst_brightness[1] = Float.parseFloat(object.getValue()[11]);
					break;
				case POWER:
					power = Integer.parseInt(object.getValue()[0]);
					break;
				case AC_STANDBY_STATUS:
					ac_standby_status = Integer.parseInt(object.getValue()[0]);
					break;
				case DC_STANDBY_STATUS:
					dc_standby_status = Integer.parseInt(object.getValue()[0]);
					break;
				}

				if(object.getName().equals(INST_BRIGHTNESS)) {

					inst_brightness[0] = Float.parseFloat(object.getValue()[10]);
					inst_brightness[1] = Float.parseFloat(object.getValue()[11]);

				}

			}
		};

		iface.includeDataRef(INST_BRIGHTNESS);
		iface.includeDataRef(POWER);
		iface.includeDataRef(AC_STANDBY_STATUS);
		iface.includeDataRef(DC_STANDBY_STATUS);
		iface.observeDataRef(INST_BRIGHTNESS, generic);
		iface.observeDataRef(POWER, generic);
		iface.observeDataRef(AC_STANDBY_STATUS, generic);
		iface.observeDataRef(DC_STANDBY_STATUS, generic);
	}
	
	private String decode(String codedString) {
		byte[] decodedBytes = Base64.getDecoder().decode(codedString);
		String decodedString = new String(decodedBytes);
		return decodedString.toUpperCase();
	}
}
