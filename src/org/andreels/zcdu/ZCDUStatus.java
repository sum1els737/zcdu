/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zcdu;

public class ZCDUStatus {
    public static final String STATUS_STARTUP = "Starting Up...";
    public static final String STATUS_NOT_CONNECTED = "Not Connected";
    public static final String STATUS_CONNECTED = "Connected";
    public static final String STATUS_SHUTDOWN = "Shutdown";

    public static final String STATUS_NAV_DB_LOADED = "NAV Databases loaded";
    public static final String STATUS_NAV_DB_NOT_LOADED = "NAV Databases not loaded";
    public static final String STATUS_NAV_DB_NOT_FOUND = "NAV Databases not found";

    public static final String STATUS_EGPWS_DB_LOADED = "EGPWS Databases loaded";
    public static final String STATUS_EGPWS_DB_NOT_LOADED = "EGPWS Databases not loaded";
    public static final String STATUS_EGPWS_DB_NOT_FOUND = "EGPWS Databases not found";
    
    public static String status = STATUS_STARTUP;
    public static String nav_db_status = STATUS_NAV_DB_NOT_LOADED;
    public static String nav_db_cycle = "";
    
    public static String cStatus = "";
    public static String wStatus = "";
    public static String navStatus = "";
    public static String egpwsStatus = "";
    
    public static String egpws_db_status = STATUS_EGPWS_DB_LOADED;
    public static String weather_status = STATUS_STARTUP;
    
    public static boolean receiving = false;
    public static boolean weather_receiving = false;
    
    public static boolean cptoutdb_running = false;
    public static boolean cptindb_running = false;
    public static boolean fooutdb_running = false;
    public static boolean foindb_running = false;
    public static boolean uppereicas_running = false;
    public static boolean lowereicas_running = false;
    
}
